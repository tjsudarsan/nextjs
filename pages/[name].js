import React from 'react';
import Head from 'next/head';

export default (props) => (
  <>
    <Head>
      <title>Create Next App</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <h1>
      Hello
      {props.name}
    </h1>
  </>
);

export const getServerSideProps = async ({ params }) => ({
  props: {
    ...params,
  },
});
